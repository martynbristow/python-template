""" Tests for app
"""
import unittest

from my_app import app


class TestMyApp(unittest.TestCase):
    """ Minimal Unittest Test Case
    """
    def test_myapp(self):
        """ Minimal Unittest"""
        self.assertEqual('Hello tester', app.hello('tester'))


if __name__ == '__main__':
    unittest.main()
