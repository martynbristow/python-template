""" App
"""


def hello(name='Anonymous'):
    """ Simple Function that Does Nothing
    """
    return "Hello {name}".format(name=name)
