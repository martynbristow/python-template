# Python Template App for Flask

**Template for Creating new Python Flask Apps with Minimal Setup**

- Supporting Python 2.7 and Python 3.7
- Using tox as a test runner

Deployment Pattern for Google Cloud Functions, Docker ... and more to come

## Usage

To use this as a template either clone the repo or download the output as a zip.

Simply customise by renaming `my_app` to whatever your app is and update:

 - `my_app/unit_test/test_my_app.py`
 - `.tox.ini`
 
## Deploy GCP

```
deploy:gcp:
  stage: deploy
  image: node
  before_script:
    - npm install --save-dev gcx -g
  script:
    - echo "${CLOUD_FN_DEPLOY}" > keys.json
    - export GOOGLE_APPLICATION_CREDENTIALS="./keys.json"
    - gcx deploy $FN_NAME --region $GCP_REGION --runtime python37 --trigger-http --allow-unauthenticated
  only:
    - master
```

## Contributing


